const routes = [
  {
    name: 'Dashboard',
    path: '/',
    component: require('@/pages/dashboard.vue').default
  },
  {
    name: 'resource',
    path: '/resource/:model',
    component: require('@/pages/resource.vue').default
  },
  {
    name: 'new-resource',
    path: '/resource/:model/new',
    component: require('@/pages/new.vue').default
  },
  {
    name: 'update-resource',
    path: '/resource/:model/update/:id',
    component: require('@/pages/update.vue').default
  },
  {
    name: 'not-found',
    path: '*',
    component: {template: '<h1>404  </h1>'}
  }
]
export default routes
