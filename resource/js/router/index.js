import VueRouter from 'vue-router'
import Vue from 'vue'
import routes from './routes'


Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  base: window.config.base,
  routes: routes // short for `routes: routes`
})


export default router
