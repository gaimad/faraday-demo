import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'
Vue.use(Vuex)
const vuexPersist = new VuexPersist({
  key: window.config.name,
  storage: localStorage
})

import resource from './resource'
import global from './global'

let modules = {
  global,
  resource
}
const store = new Vuex.Store({
  modules: modules,
  strict: false,
  plugins: [vuexPersist.plugin]
})

export default  store
