import {
} from './mutations-types'
import Api from '@/api'

const state = {
  statusModal: false,
  id: ''

}
const getters = {

}
const actions = {

}

const mutations = {
  statusModal(state, value) {
    state.statusModal = value
  },
  setId(state, value) {
    state.id = value
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
