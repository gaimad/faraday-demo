<!doctype html>
<html lang="en">
  @include('faraday::layouts.header')
  <body>
    <nav class="navbar navbar-light bg-light">
      <a class="navbar-brand" href="#">Navbar</a>
    </nav>
    <div class="container-fluid">
      <div class="row flex-xl-nowrap">
        @include('faraday::components.side-bar')
        <div id="faraday">
          <el-components></el-components>
        </div>
      </div>
    </div>
    @include('faraday::layouts.footer')
  </body>
</html>
