<?php

Route::group(['namespace' => 'Faraday\Platform\Controllers'], function() {
	Route::get('/faraday/{path?}', 'IndexController@index')->where('path', '.*');
});
