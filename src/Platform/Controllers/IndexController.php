<?php

namespace Faraday\Platform\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Faraday\Platform\Traits\TraitResource;
use Faraday\Faraday;
use Faraday\Platform\Requests\RequestResource;

class IndexController extends Controller
{

    use TraitResource;

    function __construct () {
      if(request()->route('resource') != null) {
        Faraday::getResource(request()->route('resource'));
      }
    }

    public function index (){
    	return view('faraday::template');
    }

    //
    public function list ($slug) {
      $resources = $this->get();
      $rows = $this->rows();
      $info = $this->info();
      return response()->json(['info' => $info, 'resources' => $resources, 'rows' => $rows]);
    }

    public function new ($slug) {
      $fields = $this->fields();
      return response()->json(['fields' => $fields]);
    }

    public function create (RequestResource $request, $slug) {
      $class = Faraday::$resource;
      $model = $class::$model;
      (new $model)->create($request->all());
      return response()->json(['success' => true, 'slug' => $slug]);
    }

    public function show (Request $request, $slug) {
      $fields = $this->showResource($request->id);
      return response()->json(['fields' => $fields]);
    }

    public function update (RequestResource $request, $slug) {
      $class = Faraday::$resource;
      $model = $class::$model;
      (new $model)->where('id', $request->id)->update($request->all());
      return response()->json(['success' => true, 'slug' => $slug]);
    }

    public function delete (Request $request, $slug) {
      $class = Faraday::$resource;
      $model = $class::$model;
      (new $model)->where('id', $request->id)->delete();
      return response()->json(['success' => true, 'slug' => $slug, 'id'=> $request->all()]);
    }
}
