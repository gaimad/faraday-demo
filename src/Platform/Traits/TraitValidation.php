<?php
namespace Faraday\Platform\Traits;
use Faraday\Faraday;

trait TraitValidation {

  public function validationRules () {
    $class = Faraday::$resource;
    return ['rules' => $class::$rules, 'messages' => $class::$messages];
  }

}
