<?php
namespace Faraday\Platform\Traits;
use Faraday\Faraday;

trait TraitResource {

  public function get () {
    $class = Faraday::$resource;
    $model = $class::$model;
    $columns = ['id'];
    $fields = $class->fields();
    if(count($fields) > 0) {
      foreach($fields as $field) {
        $columns [] = $field->column;
      }
      return (new $model)->get($columns);
    }
    return [];
  }

  public function rows () {
    $class = Faraday::$resource;
    $rows = [];
    $fields = $class->fields();
    foreach($fields as $field) {
      $rows [] = ['name' => $field->name, 'column' => $field->column, 'hideInTable' => $field->hideInTable];
    }
    return $rows;
  }

  public function info () {
    $class = Faraday::$resource;
    return ['group' => $class::$group, 'label' => $class::$menu_label, 'slug' => $class->getKeyUrl()];
  }

  public function fields () {
    $class = Faraday::$resource;
    $fields = $class->fields();
    $components = [];
    foreach($fields as $field) {
      $component = ['name' => $field->name, 'column' => $field->column, 'type' => $field->type];
      if($field->defaultValue != false)
        $component['value'] = $field->defaultValue;
      $components [] = $component;
    }
    return $components;
  }

  public function showResource ($id) {
    $class = Faraday::$resource;
    $model = $class::$model;
    $columns = ['id'];
    $fields = $class->fields();
    if(count($fields) > 0) {
      foreach($fields as $field) {
        $component = ['name' => $field->name, 'column' => $field->column, 'type' => $field->type];
        if($field->defaultValue != false)
          $component['value'] = $field->defaultValue;
        $components [] = $component;
      }
      $resource = (new $model)->find($id);
      foreach($components as $key => $component) {
        if($resource[$component['column']]) {
          $components[$key]['value'] = $resource[$component['column']];
        }
      }
    }
    return $components;
  }

}
