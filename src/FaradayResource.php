<?php
namespace Faraday;
use Illuminate\Support\Str;

abstract class FaradayResource
{
  function __construct() {
      $this->getKeyUrl();
  }
  public static $group = 'Other';

  public static $menu_label = '';

  public static $model = '';

  public static $rules = [];

  public static $messages = [];

  abstract public function fields();

  public static function getKeyUrl () {
    return Str::slug(Str::afterLast(get_called_class(), '\\'));
  }


}
