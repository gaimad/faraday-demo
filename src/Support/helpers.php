<?php

if(!function_exists('sideBarDashboard')) {

  function sideBarDashboard() {
    $MyClasses = Faraday\Faraday::$resources;
    $sideBarItemsByGroup = [];
    foreach($MyClasses as $class)
    {
      if($class::$menu_label != '') {
        $sideBarItemsByGroup [$class::$group][] = ['label' => $class::$menu_label, 'slug' => $class->getKeyUrl()];
      }
    }
    return $sideBarItemsByGroup;
  }

}
