<?php
namespace Faraday;
use Symfony\Component\Finder\Finder;

class Faraday {

  public static $resources = [];
  public static $resource = null;

  public static function resourcesInFolder () {
    $resourcesINFolder = collect((new Finder())->files()->in(app_path().'/'.config('faraday.path_resource', 'Faraday/')));
    $resource = [];
    foreach($resourcesINFolder as $resource) {
      $namespace = app()->getNamespace();
      $resource = $namespace.str_replace(['/', '.php'], ['\\', ''], config('faraday.path_resource', 'Faraday/').$resource->getBasename());
      if(is_subclass_of(new $resource, FaradayResource::class)) {
        $resources [] = new $resource;
      }
    }
    static::$resources = $resources;
    return new static;
  }


  public static function getResource ($slug) {
    $resources = self::$resources;

    foreach($resources as $resource)
    {
      if($resource::getKeyUrl() == $slug) {
        self::$resource = $resource;
        return new static;
      }
    }
  }

  public static function jsonData () {
    return [
      'base' => config('faraday.path', '/faraday'),
      'path' => request()->root(),
      'name' => config('faraday.name', 'faraday')
    ];
  }
}
