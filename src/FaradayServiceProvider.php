<?php

namespace Faraday\src;

use Illuminate\Support\ServiceProvider;
use Symfony\Component\Finder\Finder;
use Faraday\Faraday;

class FaradayServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
      if (file_exists($file = __DIR__.'/Support/helpers.php'))
      {
          require $file;
      }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/Platform/config/faraday.php' => config_path('faraday.php')
        ]);
        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/faraday'),
        ], 'public');
        $this->loadRoutesFrom(__DIR__.'/Platform/routes.php');
        $this->loadRoutesFrom(__DIR__.'/Platform/api.php');
        $this->loadViewsFrom(__DIR__.'/Platform/views', 'faraday');
        Faraday::resourcesInFolder();
    }


}
